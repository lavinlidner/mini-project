<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\User;
use AppBundle\Entity\Customer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CustomerController extends Controller
{
    /**
     * @Route("customer/new", name="new_address")
     */
    public function newAction(Request $request)
    {
        $address = new Customer();
        $form = $this->createFormBuilder($address)
        // ->add('name', TextType::class)
        ->add('address', TextType::class)
        ->add('save', SubmitType::class, array('label' => 'Submit'))
        ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $address = $form->getData();
            $doct = $this->getDoctrine()->getManager();

            
            $address->setUser($this->getUser());
            $address->setName($this->getUser()->getName());
            
            $doct->persist($address);

            $doct->flush();

            return $this->redirectToRoute('address_list');
        } else {
            return $this->render('customer/new.html.twig', array(
                'form' => $form->createView(),
            ));
        }
        // return $this->render('customer/new.html.twig', array(
        //     'form' => $form->createView(),
        //     'test' => $this->getUser()->getName(),
        // ));
    }

    /**
     * @Route("/customer", name="address_list")
     */
    public function displayAction()
    {
        $user = $this->getUser();
        
        $customer = $this->getDoctrine()
            ->getRepository('AppBundle:Customer')
            ->findByUser($user);

            return $this->render('customer/display.html.twig', array('data' => $customer));
    }

    /**
     * @Route("/customer/update/{id}", name="address_update")
     */
    public function updateAction($id, Request $request)
    {
        $doct = $this->getDoctrine()->getManager();
        $user = $doct->getRepository('AppBundle:Customer')->find($id);

        if (!$user) {
            throw $this->createNotFoundException(
                'No customer found for id '.$id
            );
        }
        $form = $this->createFormBuilder($user)
        ->add('address', TextType::class)
        ->add('save', SubmitType::class, array('label' => 'Submit'))
        ->getForm();

        $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $user = $form->getData();
                $doct = $this->getDoctrine()->getManager();

                $doct->persist($user);

                $doct->flush();

                return $this->redirectToRoute('address_list');
            }
        return $this->render('customer/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("customer/delete/{id}", name="customer_delete")
     */
    public function deleteAction($id) {
        $doct = $this->getDoctrine()->getManager();
        $user = $doct->getRepository('AppBundle:Customer')->find($id);

        if (!$user) {
            throw $this->createNotfoundException('No customer found for id '.$id);
        }
        $doct->remove($user);
        $doct->flush();
        return $this->redirectToRoute('address_list');
    }
}