<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Product;
use AppBundle\Entity\Cart;
use AppBundle\Entity\CartProduct;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CartController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('cart/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ));
    }

    // /**
    //  * @Route("/cart/add", name="add_to_cart")
    //  */
    // public function addProductToCartAction(Request $request)
    // {
    //     if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
    //         throw $this->createAccessDeniedException();
    //     }

    //     $user = $this->getUser();

    //     $product = new Cart();
    //         $form = $this->createFormBuilder($product)
    //         ->add('product', EntityType::class, array(
    //             'class' => 'AppBundle:Product',
    //             'choice_label' => 'id',
    //             'multiple' => false,
    //         ))
    //         ->add('quantity', TextType::class)
    //         ->add('save', SubmitType::class, array('label' => 'Submit'))
    //         ->getForm();

    //         $form->handleRequest($request);

    //         if ($form->isSubmitted()  && $form->isValid()) {
    //             $product = $form->getData();
    //             $doct = $this->getDoctrine()->getManager();

    //             // tells Doctrine you want to save the Product
    //             $doct->persist($product);

    //             // executes the queries (i.e. the INSERT query)
    //             $doct->flush();

    //             return $this->redirectToRoute('cart_display');
    //         } 
    //     return $this->render('cart/new.html.twig', array(
    //         'form' => $form->createView(),
    //     ));
    // }

    /**
     * @Route("/cart", name="cart_display")
     */
    public function displayAction()
    {
        $user = $this->getUser();
        
        $product = $this->getDoctrine()
            ->getRepository('AppBundle:Cart')
            ->findByUser($user);

        $cartproducts = $this->getDoctrine() 
            ->getRepository('AppBundle:CartProduct')
            ->findByCart($product);

        return $this->render('cart/show.html.twig', array('data' => $product));
    }

    /**
     * @Route("/cart/update/{id}", name="cart_update")
     */
    public function updateAction($id, Request $request)
    {
        $doct = $this->getDoctrine()->getManager();
        $product = $doct->getRepository('AppBundle:CartProduct')->find($id);

        if (!$product) {
            throw $this->createNotFoundException(
                'No cart found for id '.$id
            );
        }
        $form = $this->createFormBuilder($product)
        ->add('product', EntityType::class, array(
            'class' => 'AppBundle:Product',
            'choice_label' => 'name',
            'multiple' => false,
        ))
        ->add('quantity', TextType::class)
        ->add('save', SubmitType::class, array('label' => 'Submit'))
        ->getForm();

        $form->handleRequest($request);

            if ($form->isSubmitted()  && $form->isValid()) {
                $product = $form->getData();
                $doct = $this->getDoctrine()->getManager();

                // tells Doctrine you want to save the Product
                $doct->persist($product);

                // executes the queries (i.e. the INSERT query)
                $doct->flush();

                return $this->redirectToRoute('cart_display');
            } 
        return $this->render('cart/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/cart/delete/{id}", name="cart_delete")
     */
    public function deleteAction($id) {
        $doct = $this->getDoctrine()->getManager();
        $product = $doct->getRepository('AppBundle:Cart')->find($id);

        if (!$product) {
            throw $this->createNotFoundException('No product found for id ',$id);
        }
        $doct->remove($product);
        $doct->flush();
        return $this->redirectToRoute('cart_display');
    }

    /**
     * @Route("/cartproduct/add", name="add_to_cartproduct")
     */
    public function addCartToProductCart(Request $request)
    {
        $cart = new ProductCart();
            $form = $this->createFormBuilder($cart)
            ->add('product', Entitytype::class, array(
                'class' => 'AppBundle:Product',
                'choice_label' => 'name',
                'multiple' => false,
            ))
            ->add('quantity', TextType::class)
            ->add('Total Price', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Submit'))
            ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $cart = $form->getData();
                $doct = $this->getDoctrine()->getManager();

                $doct->persist($cart);
                $doct->flush();

                return $this->redirectToRoute('cartproduct_display');
            }
        return $this->render('cartproduct/new.html.twig', array(
            'form'
        ));
    }
}