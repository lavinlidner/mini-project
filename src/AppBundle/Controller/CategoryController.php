<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Product;
use AppBundle\Entity\Category;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CategoryController extends Controller
{
    /**
     * @Route("/category/new", name="category_new")
     */
    // public function newAction(Request $request)
    // {
    //     $category = new Category();
    //         $form = $this->createFormBuilder($category)
    //         ->add('name', TextType::class)
    //         ->add('save', SubmitType::class, array('label' => 'Submit'))
    //         ->getForm();
        
    //         $form->handleRequest($request);

    //         if ($form->isSubmitted()  && $form->isValid()) {
    //             $category = $form->getData();
    //             $doct = $this->getDoctrine()->getManager();

    //             // tells Doctrine you want to save the Product
    //             $doct->persist($category);

    //             // executes the queries (i.e. the INSERT query)
    //             $doct->flush();

    //             return $this->redirectToRoute('category_display');
    //         } else {
    //             return $this->render('category/new.html.twig', array(
    //                 'form' => $form->createView(),
    //             ));
    //         }    
    //     }

    /**
     * @Route("/category", name="category_display")
     */
    public function displayAction()
    {
        $category = $this->getDoctrine() 
            ->getRepository('AppBundle:Category')
            ->findAll();

        $product = $this->getDoctrine()
            ->getRepository('AppBundle:Product')
            ->findAll();
            
            return $this->render('category/display.html.twig', array('data' => $category));
    }

    /**
     * @Route("/category/update/{id}", name="category_update")
     */
    public function updateAction($id, Request $request) {
        $doct = $this->getDoctrine()->getManager();
        $category = $doct->getRepository('AppBundle:Category')->find($id);

        if (!$category) {
            throw $this->createNotFoundException(
                'No category found for id '.$id
            );
        }
        $form = $this->createFormBuilder($category)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Submit'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form = $form->getData();
            $doct = $this->getDoctrine()->getManager();

            // tells Doctrine you want to save the Product
            $doct->persist($category);

            // executes the queries (i.e. the INSERT query)
            $doct->flush();
            return $this->redirectToRoute('category_display');
        } else {
            return $this->render('category/new.html.twig', array(
                'form' => $form->createView(),
            ));
        }
    }

    /**
     * @Route("/category/delete/{id}", name="category_delete")
     */
    public function deleteAction($id) {
        $doct = $this->getDoctrine()->getManager();
        $category = $doct->getRepository('AppBundle:Category')->find($id);

        if (!$category) {
            throw $this->createNotFoundException('No category found for id ',$id);
        }
        $doct->remove($category);
        $doct->flush();
        return $this->redirectToRoute('category_display');
    }
}