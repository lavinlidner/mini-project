<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Entity\Cart;
use AppBundle\Entity\CartProduct;
use AppBundle\Entity\SalesOrder;
use AppBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CheckoutController extends Controller
{
    // /**
    //  * @Route("/order/add", name="add_to_order")
    //  */
    // public function addCartToOrderAction(Request $request)
    // {
    //     $cartproduct = new Checkout();
    //         $form = $this->createFormBuilder($cart)
    //         ->add('product', EntityType::class, array(
    //             'class' => 'AppBundle:Product',
    //             'choice_label' => 'name',
    //             'multiple' => 'false'
    //         ))
    //         ->add('quantity', TextType::class)
    //         ->add('price', TextType::class)
    //         ->add('save', SubmitType::class, array('label' => 'Submit'))
    //         ->getForm();

    //         $form->handleRequest($request);

    //         if ($form->isSubmitted() && $form->isValid()) {
    //             $cart = $form->getData();
    //             $doct = $this->getDoctrine()->getManager();

    //             $doct->persist($cart);
    //             $doct->flush();

    //             return $this->redirectToRoute('order_display');
    //         }
    //     return $this->render('order/new.html.twig', array(
    //         'form' => $form->createView(),
    //     ));
    // }

    /**
     * @Route("/checkout", name="cart_checkout")
     */
    public function checkout(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $user = $this->getUser();

        $cart = $this->getDoctrine()
            ->getRepository('AppBundle:Cart')
            ->findByUser($user);

        $cartproducts = $this->getDoctrine() 
            ->getRepository('AppBundle:CartProduct')
            ->findByCart($cart);

        return $this->render('checkout/index.html.twig', array('data' => $cartproducts));

        // $users = $this->getDoctrine()->getEntityManager()
        //             ->createQuery('SELECT u FROM AppBundle:User u')
        //             ->getResult();

        // $result = array();

        // foreach($users as $user){
        //     if($user->hasRole('ROLE_CUSTOMER'))
        //         $result[] = $user;
        // }

        // return $this->render('checkout/index.html.twig', array(
        //     'users' => $result
        // ));
    }

    // /**
    //  * @Route("/order", name="order")
    //  */
    // public function orderAction()
    // {
    //     $cart = $this->getDoctrine()
    //         ->getRepository('AppBundle:Order')
    //         ->findAll();

    //     return $this->render('order/show.html.twig', array('data' => $cart));
    // }

    /**
     * @Route("/checkout/delete/{id}", name="checkout_delete")
     */
    public function deleteAction($id) {
        $doct = $this->getDoctrine()->getManager();

        $cartproducts = $doct->getRepository('AppBundle:CartProduct')->find($id);

        $cart = $doct->getRepository('AppBundle:Cart')->findOneById($cartproducts->getCart()->getId());

        $product = $doct->getRepository('AppBundle:Product')->findOneById($cartproducts->getProduct()->getId());
        // dump($cart);
        // dump($product);
        // die();

        if (!$cartproducts) {
            throw $this->createNotFoundException('No product found for id ',$id);
        }
        $doct->remove($cartproducts);

        $doct->flush();
        
        $cart->setQuantity($cart->getQuantity() - $cartproducts->getQuantity());
        $cart->setTotalPrice($cart->getTotalPrice() - ($cartproducts->getQuantity() * $product->getPrice()));

        $doct->persist($cart);

        $doct->flush();

        $product->setQuantityHold($product->getQuantityHold() - $cartproducts->getQuantity());

        $doct->persist($product);

        $doct->flush();
        
        return $this->redirectToRoute('cart_checkout');
    }
}