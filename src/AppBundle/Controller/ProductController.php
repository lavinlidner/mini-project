<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use AppBundle\Entity\Product;
use AppBundle\Entity\Category;
use AppBundle\Entity\Cart;
use AppBundle\Entity\CartProduct;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ProductController extends Controller
{
    /**
     * @Route("/home")
     */
    public function homeAction()
    {
        return $this->render('product/home.html.twig');
    }
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ));
    }

    /**
     * @Route("product/new", name="product_new")
     */
    public function newAction(Request $request)
    {
        $product = new Product();
            $form = $this->createFormBuilder($product)
            ->add('name', TextType::class)
            ->add('description', TextType::class)
            ->add('price', TextType::class)
            ->add('quantity', TextType::class)
            ->add('category', EntityType::class, array(
                // looks for choices from this entity
                'class' => 'AppBundle:Category',

                // uses the User.username property as the visible option string
                'choice_label' => 'name',

                // used to render a select box, check boxes or radios
                // 'expanded' => true,
                'multiple' => false,
            ))
            ->add('save', SubmitType::class, array('label' => 'Submit'))
            ->getForm();
        
            $form->handleRequest($request);

            if ($form->isSubmitted()  && $form->isValid()) {
                $product = $form->getData();
                $doct = $this->getDoctrine()->getManager();

                $product->setQuantityHold(0);

                // tells Doctrine you want to save the Product
                $doct->persist($product);

                // executes the queries (i.e. the INSERT query)
                $doct->flush();

                return $this->redirectToRoute('product_display');
            } else {
                return $this->render('product/new.html.twig', array(
                    'form' => $form->createView(),
                ));
            }    
        }

    /**
     * @Route("/product", name="product_display")
     */
    public function displayAction()
    {
        $product = $this->getDoctrine() 
            ->getRepository('AppBundle:Product')
            ->findBy([], ['name' => 'ASC']);

        $category = $this->getDoctrine()
            ->getRepository('AppBundle:Category')
            ->findAll();

            return $this->render('product/display.html.twig', array('data' => $product));
    }

    /**
     * @Route("/product/update/{id}", name="product_update")
     */
    public function updateAction($id, Request $request) {
        $doct = $this->getDoctrine()->getManager();
        $product = $doct->getRepository('AppBundle:Product')->find($id);

        if (!$product) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }
        $form = $this->createFormBuilder($product)
            ->add('name', TextType::class)
            ->add('description', TextType::class)
            ->add('price', TextType::class)
            ->add('quantity', TextType::class)
            ->add('category', EntityType::class, array(
                // looks for choices from this entity
                'class' => 'AppBundle:Category',

                // uses the User.username property as the visible option string
                'choice_label' => 'name',

                // used to render a select box, check boxes or radios
                // 'expanded' => true,
                'multiple' => false
            ))
            ->add('save', SubmitType::class, array('label' => 'Submit'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form = $form->getData();
            $doct = $this->getDoctrine()->getManager();

            // tells Doctrine you want to save the Product
            $doct->persist($product);

            // executes the queries (i.e. the INSERT query)
            $doct->flush();
            return $this->redirectToRoute('product_display');
        } else {
            return $this->render('product/new.html.twig', array(
                'form' => $form->createView(),
            ));
        }
    }

    /**
     * @Route("/product/delete/{id}", name="product_delete")
     */
    public function deleteAction($id) {
        $doct = $this->getDoctrine()->getManager();
        $product = $doct->getRepository('AppBundle:Product')->find($id);

        if (!$product) {
            throw $this->createNotFoundException('No product found for id ',$id);
        }
        $doct->remove($product);
        $doct->flush();
        return $this->redirectToRoute('product_display');
    }

    /**
     * @Route("/product/{id}", name="product_detail")
     */
    public function detailAction($id, Request $request) {
        $product = $this->getDoctrine() 
            ->getRepository('AppBundle:Product')
            ->find($id);

        if(!$product){
            throw $this->createNotFoundException(
                'No product found for id'.$id
            );
        }

        // return new Response('Check out this great product: '.$product->getName());

        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        
        $doct = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $cart = new Cart();
            $form = $this->createFormBuilder($cart)
            ->add('quantity', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Submit'))
            ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted()  && $form->isValid()) {

                $exsit_cart = $this->getDoctrine()
                    ->getRepository('AppBundle:Cart')
                    ->findByUser($user);

                $dataproduct = $this->getDoctrine() 
                    ->getRepository('AppBundle:Product')
                    ->find($id);

                $cartform = $form->getData();

                if(!$exsit_cart) {

                    // kalau cart belom ada
                    $cart->setUser($user);
                    $doct = $this->getDoctrine()->getManager();
                    $price = $product->getPrice();
                    $cart->setTotalPrice($price * $cartform->getQuantity());
                    
                    $doct->persist($cart);

                    $doct->flush();

                    $cart->getId();
                    $cartproduct = new CartProduct();
                    // $dataproduct = $this->getDoctrine() 
                    //     ->getRepository('AppBundle:Product')
                    //     ->find($id);
                    
                    $cartproduct->setProduct($dataproduct);
                    $cartproduct->setCart($cart);
                    $cartproduct->setTotalPrice($cart->getTotalPrice());
                    $cartproduct->setQuantity($cartform->getQuantity());

                    $doct->persist($cartproduct);

                    $doct->flush();
                } // kalau cart sudah ada 
                else 
                {
                    //kalau cart sudah ada
                    $cart = $exsit_cart[0];
                    $cekcart = $this->getDoctrine()
                        ->getRepository('AppBundle:CartProduct')
                        ->find($cart);
    
                    
                    // dump($cart->getTotalPrice());
                    // dump($product->getPrice());
                    // die();

                    $exsit_cartproduct = $this->getDoctrine()
                        ->getRepository('AppBundle:CartProduct')
                        ->findOneBy(array('cart' => $cart, 'product' => $product));
                        // kalau product belom ada di cartproduct
                        if(!$exsit_cartproduct) {
                            $cartproduct = new CartProduct();
                            $cartproduct->setQuantity($cartform->getQuantity());
                            $cartproduct->setTotalPrice($product->getPrice() * $cartform->getQuantity());
                            $cartproduct->setProduct($dataproduct);
                            $cartproduct->setCart($cart);
            
                            $doct->persist($cartproduct);
            
                            $doct->flush();
                        } // kalau product ada di cartproduct
                        else  {
                            $exsit_cartproduct->setTotalPrice($exsit_cartproduct->getTotalPrice() + ($product->getPrice() * $cartform->getQuantity()));

                            $exsit_cartproduct->setQuantity($exsit_cartproduct->getQuantity() + $cartform->getQuantity());

                            $doct->persist($exsit_cartproduct);

                            $doct->flush();
                        }
                    $cart->setTotalPrice($cart->getTotalPrice() + ($product->getPrice() * $cartform->getQuantity()));
                    $cart->setQuantity($cart->getQuantity() + $cartform->getQuantity());
                    $doct->persist($cart);
    
                    $doct->flush();
                }
                $product->setQuantityHold($product->getQuantityHold() + $cartform->getQuantity());
                $product->setQuantity($product->getQuantity() - $cartform->getQuantity());
                    // dump($product->getQuantityHold());
                    // die();

                    $doct->persist($product);

                    $doct->flush();
                return $this->redirectToRoute('cart_checkout');
            } 
        return $this->render('cart/new.html.twig', array(
            'form' => $form->createView(),
            ));
        }     
}
