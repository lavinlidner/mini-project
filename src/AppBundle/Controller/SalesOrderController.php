<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\SalesOrder;
use AppBundle\Entity\SalesOrderProduct;
use AppBundle\Entity\Cart;
use AppBundle\Entity\CartProduct;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class SalesOrderController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    // public function indexAction(Request $request)
    // {
    //     // replace this example code with whatever you need
    //     return $this->render('salesorder/index.html.twig', array(
    //         'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
    //     ));
    // }

    /**
     * Bikin Order ngambil dari Cart
     * flow
     * Order ambil dari Cart
     * apa aja yang di ambil
     * ID Cart
     * user_id
     * total_harga di DB SalesOrder namanya total_amount
     * 
     * @Route("/order", name="order")
     */
    public function order(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $doct = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $cart = $this->getDoctrine()
            ->getRepository('AppBundle:Cart')
            ->findOneByUser($user);

        $cartproducts = $this->getDoctrine()
            ->getRepository('AppBundle:CartProduct')
            ->findByCart($cart);

        $order = new SalesOrder();
        $order->setUser($user);
        $order->setTotalAmount($cart->getTotalPrice());

        $order->setStatus('Waiting Payment');

        $doct->persist($order);

        $doct->flush();



        foreach ($cartproducts as $item) {
            $orderdetail = new SalesOrderProduct();
            $orderdetail->setProduct($item->getProduct());
            $orderdetail->setTotalPrice($item->getTotalPrice());
            $orderdetail->setQuantity($item->getQuantity());
            $orderdetail->setSalesOrder($order);

            $doct->persist($orderdetail);

            $doct->flush();

            $doct->remove($item);

            $doct->flush();        

            $product = $this->getDoctrine()->getManager()
                ->getRepository('AppBundle:Product')
                ->findOneById($item->getProduct()->getId());
                
            $product->setQuantityHold($product->getQuantityHold() - $item->getQuantity());
                
            // dump($product);
            // die();
        }

        $doct->remove($cart);

        $doct->flush();

        return $this->redirectToRoute('transaction_list');
    }

    /**
     * @Route("/transaction", name="transaction_list")
     */
    public function listAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $user = $this->getUser();

        $transaction = $this->getDoctrine()
            ->getRepository('AppBundle:SalesOrder')
            ->findByUser($user);

        return $this->render('transaction/show.html.twig', array('data' => $transaction));
    }

    /**
     * @Route("/transaction/detail", name="transaction_detail")
     */
    public function detailAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $user = $this->getUser();

        $salesorder = $this->getDoctrine()
            ->getRepository('AppBundle:SalesOrder')
            ->findByUser($user);

        // $salesorderproduct = new SalesOrderProduct();

        $salesorderproduct = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:SalesOrderProduct')
            ->findBySalesOrder($salesorder);

        // dump($salesorderproduct);
        // die();

        return $this->render('transaction/detail.html.twig', array('data' => $salesorderproduct));
    }

    /**
     * @Route("/order/status", name="order_status")
     * @Security("has_role('ROLE_SELLER')")
     */
    public function statusAction(Request $request)
    {
        // if (!$this->get('security.authorization_checker')->isGranted('IS_AUNTHENTICATED_FULLY')) {
        //     throw $this->createAccessDeniedException();
        // }

        $salesorder = $this->getDoctrine()
            ->getRepository('AppBundle:SalesOrder')
            ->findAll(); 
            
        return $this->render('transaction/status.html.twig', array('data' => $salesorder));
    }

    /**
     * @Route("/order/status/approved/{id}", name="order_status_approved")
     */
    public function approvedAction(SalesOrder $salesorder)
    {
        $doct = $this->getDoctrine()->getManager();

        $salesorder->setStatus('Approved');
        // dump($salesorder);
        // die();

        $doct->persist($salesorder);

        $doct->flush();

        return $this->render('transaction/approved.html.twig', array('data' => $salesorder));
    }

    /**
     * @Route("/order/status/reject/{id}", name="order_status_reject")
     */
    public function rejectAction(SalesOrder $salesorder)
    {
        $doct = $this->getDoctrine()->getManager();

        $salesorder->setStatus('Reject');

        $doct->persist($salesorder);

        $doct->flush();

        $salesorderproduct = $doct->getRepository('AppBundle:SalesOrderProduct')
            ->findBySalesOrder($salesorder);

        foreach($salesorderproduct as $item)
        {
            $product = $doct->getRepository('AppBundle:Product')
                ->findOneById($item->getProduct()->getId());

            $product->setQuantity($product->getQuantity() + $item->getQuantity());

            $doct->persist($product);

            $doct->flush();
        }

        $doct->persist($salesorder);

        $doct->flush();

        return $this->render('transaction/reject.html.twig', array('data' => $salesorder));
    }
}