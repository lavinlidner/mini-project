<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SalesOrderProduct
 *
 * @ORM\Table(name="sales_order_product")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SalesOrderProductRepository")
 */
class SalesOrderProduct
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SalesOrder")
     * @ORM\JoinColumn(name="sales_order_id")
     */
    private $salesOrder;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Product")
     */
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(name="quantity", type="string", length=255)
     */
    private $quantity;

    /**
     * @ORM\Column(name="total_price", type="decimal", scale=2)
     */
    private $total_price;

    


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set salesorder
     */
    public function setSalesOrder(SalesOrder $salesOrder)
    {
        $this->salesOrder = $salesOrder;
        return $this;
    }

    /**
     * Get salesorder
     */
    public function getSalesOrder()
    {
        return $this->salesOrder;
    }

    /**
     * Set product
     *
     * @param string $product
     *
     * @return SalesOrderProduct
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return string
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set total_price
     */
    public function setTotalPrice($totalPrice)
    {
        $this->total_price = $totalPrice;
        return $this;
    }

    /**
     * Get total_price
     */
    public function getTotalPrice()
    {
        return $this->total_price;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     *
     * @return SalesOrderProduct
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
}

