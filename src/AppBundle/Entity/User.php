<?php
// src/AppBundle/Entity/User.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity="Customer", mappedBy="user")
     */
    private $customer;

    /**
     * @ORM\OneToOne(targetEntity="Seller", mappedBy="user")
     */
    private $seller;

    protected $roles = array();

    /**
    * @ORM\OneToOne(targetEntity="Cart", mappedBy="user", cascade={"persist"})
    */
    protected $cart;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    // other properties and methods

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * Set cart
     *
     * @param \AppBundle\Entity\Cart $cart
     * @return User
     */
    public function setCart(\AppBundle\Entity\Cart $cart = null)
    {
        $this->cart = $cart;
        return $this;
    }
    /**
     * Get cart
     *
     * @return \AppBundle\Entity\Cart 
     */
    public function getCart()
    {
        return $this->cart;
    }
}
