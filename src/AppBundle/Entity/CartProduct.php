<?php
// src/AppBundle/Entity/CartProduct.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\User;
use AppBundle\Entity\Product;
use AppBundle\Entity\Cart;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="cartproduct")
 */
class CartProduct
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="cartProducts")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="Cart", inversedBy="cartProducts")
     */
    private $cart;

    /**
     * @var integer
     *
     * @ORM\Column(name="total_price", type="integer")
     * @Assert\NotBlank(message="Please enter a price")
     * @Assert\Range(
     *     max="1000",
     *     min="1",
     *     maxMessage="The product must not be more expensive than 1000 dollars",
     *     minMessage="The product should cost at least one dollar"
     * )
     */
    private $total_price;

     /**
     * @ORM\Column(type="integer")
     * 
     * @Assert\Type(
     *      type="numeric",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $quantity;

    public function getId()
    {
        return $this->id;
    }

    public function setProduct(Product $product)
    {
        $this->product = $product;
        return $this;
    }

    public function getProduct()
    {
        return $this->product;
    }
    
    public function setCart(Cart $cart)
    {
        $this->cart = $cart;
        return $this;
    }

    public function getCart()
    {
        return $this->cart;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Product
     */
    public function setTotalPrice($totalPrice)
    {
        $this->total_price = $totalPrice;
        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getTotalPrice()
    {
        return $this->total_price;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }
}