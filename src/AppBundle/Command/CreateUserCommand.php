<?php
// src/AppBundle/Command/CreateUserCommand.php
namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class CreateUserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "app/console")
            ->setName('app:create-user')

            // configure an argument
            ->addArgument('email', InputArgument::REQUIRED, 'The email of the user.')
            ->addArgument('username', InputArgument::REQUIRED, 'The username of the user.')
            ->addArgument('name', InputArgument::REQUIRED, 'The name of the user.')
            ->addArgument('address', InputArgument::REQUIRED, 'The address of the user.')

            // the short description shown while running "php app/console list"
            ->setDescription('Creates a new user.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to create a user...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln([
            'User Creator',
            '============',
            '',
        ]);

        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Continue with this action?', false);
        $question = new Question('What your password?');
        $question->setValidator(function ($value) {
            if (trim($value) == '') {
                throw new \Exception('The password cannot be empty');
            }

            return $value;
        });
        $question->setHidden(true);
        $question->setMaxAttempts(20);
        $question->setHiddenFallback(false);

        $password = $helper->ask($input, $output, $question);

        if (!$helper->ask($input, $output, $question)) {
            return;
        }
        // outputs a message followed by a "\n"
        $output->writeln('Whoa!');

        // outputs a message without adding a "\n" at the end of the line
        $output->write('You are about to ');
        $output->writeln('create a user.');

        // retrieve the argument value using getArgument()
        $output->writeln('Email: '.$input->getArgument('email'));
        $output->writeln('Username: '.$input->getArgument('username'));
        $output->writeln('Name: '.$input->getArgument('name'));
        $output->writeln('Address: '.$input->getArgument('address'));

        // access the container using getContainer()
        // $userManager = $this->getContainer()->get('app.user_manager');
        // $userManager->create($input->getArgument('username'));

        $output->writeln('User successfully generate!');
    }
}