<?php
// src/AppBundle/Command/CreateCategoryCommand.php
namespace AppBundle\Command;

use AppBundle\Entity\Category;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class CreateCategoryCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "app/console")
            ->setName('app:create-category')

            // the short description shown while running "php app/console list"
            ->setDescription('Creates a new category.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to create category...')

            // configure an argument
            ->addArgument('category', InputArgument::REQUIRED, 'The category of the product.')
            // ->addArgument('name', InputArgument::REQUIRED, 'The name of the category.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $categoryName = $input->getArgument('category');
     
        $category = new Category;
        $category
             ->setName($categoryName)
        ;
        $em = $this->getContainer()->get('doctrine')->getManager();
        $em->persist($category);
        $em->flush();
        // $output->writeln('Success');
    }
}