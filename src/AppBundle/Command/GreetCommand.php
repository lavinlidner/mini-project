<?php
// src/AppBundle/Command/GreetCommand.php
namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GreetCommand extends Command
{
    protected $defaultName;

    public function __construct($defaultName)
    {
        $this->defaultName = $defaultName;

        parent::__construct();
    }

    protected function configure()
    {
        // try to avoid work here (e.g. database query)
        // this method is *always* called - see warning below

        $this
            ->setName('demo:greet')
            ->setDescription('Greet someone')
            // ->addOption(
            //     'name',
            //     '-n',
            //     InputOption::VALUE_REQUIRED,
            //     'Who do you want to greet?',
            //     $this->defaultName
            // )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // $name = $input->getOption('name');

        // $output->writeln($name);
    }
}