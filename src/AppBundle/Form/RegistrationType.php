<?php
// src/AppBundle/Form/RegistrationType.php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name');
        $builder->add('roles', 'choice', [
            'choices' => ['ROLE_CUSTOMER' => 'ROLE_CUSTOMER', 'ROLE_SELLER' => 'ROLE_SELLER'],
            'multiple' => true,
        ])
        ->add('save', 'submit', ['label' => 'tekan button save ini untuk nyimpan roles ke dalam DB']);
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}